package runners;

import phaser.PhaserDataChanger;

import java.util.concurrent.Phaser;

public class PhaserThread implements Runnable {
    private PhaserDataChanger phaserData;
    private Phaser phaser;
    private int data;

    public PhaserThread(PhaserDataChanger phaserDataChanger, Phaser phaser, int data) {
        this.phaser = phaser;
        this.phaser.register();
        this.data = data;
        this.phaserData = phaserDataChanger;
    }

    @Override
    public void run() {
        phaserData.doPhaserMagick(data, phaser);
    }
}
