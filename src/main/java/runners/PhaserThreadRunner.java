package runners;

import phaser.PhaserDataChanger;
import phaser.PhaserAddAndMultiply;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Phaser;

public class PhaserThreadRunner {

    private static final int THREAD_COUNT = 5;

    public long getAns() {
        PhaserDataChanger phaserDataChanger = new PhaserAddAndMultiply();
        List<Thread> threadList = getThreadsList(phaserDataChanger);

        try {
            startAndJoinAllThreads(threadList);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        return phaserDataChanger.getData();
    }

    private static void startAndJoinAllThreads(List<Thread> threadList) throws InterruptedException {
        for (Thread thread : threadList) {
            thread.start();
        }
        for (Thread thread : threadList) {
            thread.join();
        }
    }

    private static List<Thread> getThreadsList(PhaserDataChanger phaserDataChanger) {
        Phaser phaser = new Phaser();
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < THREAD_COUNT; i++) {
            Runnable runnable = new PhaserThread(phaserDataChanger, phaser, i + 1);
            threads.add(new Thread(runnable));
        }
        return threads;
    }
}
