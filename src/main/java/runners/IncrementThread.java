package runners;

import incrementators.DefaultIncrementation;

public class IncrementThread implements Runnable {

    private static final int COUNT_INCREMENTS = 10_000;
    private final DefaultIncrementation implementor;
    private static final int INCREMENTS = 1;

    public IncrementThread(DefaultIncrementation implementor) {
        this.implementor = implementor;
    }

    @Override
    public void run() {
        for (int i = 0; i < COUNT_INCREMENTS; i++) {
            implementor.incrementation(INCREMENTS);
        }
    }
}
