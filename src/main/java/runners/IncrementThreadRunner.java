package runners;

import incrementators.DefaultIncrementation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class IncrementThreadRunner {

    private static final int THREAD_COUNT = 5;

    public int getIncrementValueFromImplementation(DefaultIncrementation increment) {
        List<Thread> threadList = getThreadsList(increment);

        try {
            startAndJoinAllThreads(threadList);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        return increment.getData();
    }

    private static void startAndJoinAllThreads(List<Thread> threadList) throws InterruptedException {
        for (Thread thread : threadList) {
            thread.start();
        }
        for (Thread thread : threadList) {
            thread.join();
        }
    }

    private static List<Thread> getThreadsList(DefaultIncrementation dataImplementor) {
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < THREAD_COUNT; i++) {
            threads.add(new Thread(new IncrementThread(dataImplementor)));
        }
        return threads;
    }
}



