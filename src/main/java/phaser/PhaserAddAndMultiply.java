package phaser;

import java.util.concurrent.Phaser;
import java.util.concurrent.atomic.AtomicInteger;

public class PhaserAddAndMultiply implements PhaserDataChanger {

    private AtomicInteger i = new AtomicInteger(0);

    @Override
    public void doPhaserMagick(int count, Phaser phaser) {
        i.addAndGet(count);
        phaser.arriveAndAwaitAdvance();
        i.set(i.get() * count);
        phaser.arriveAndDeregister();
    }

    @Override
    public int getData() {
        return i.get();
    }
}
