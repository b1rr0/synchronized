package phaser;

import java.util.concurrent.Phaser;

public interface PhaserDataChanger {

    void doPhaserMagick(int count, Phaser phaser);

    int getData();
}
