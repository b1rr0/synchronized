package incrementators;

public class DefaultIntInc implements DefaultIncrementation {

    private int i = 0;

    @Override
    public void incrementation(int count) {
        i += count;
    }

    @Override
    public int getData() {
        return i;
    }

}
