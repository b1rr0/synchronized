package incrementators;

public class SyncBlockInc implements DefaultIncrementation {

    private Integer i = 0;

    @Override
    public void incrementation(int count) {
        synchronized (this) {
            i += count;
        }
    }

    @Override
    public int getData() {
        return i;
    }

}
