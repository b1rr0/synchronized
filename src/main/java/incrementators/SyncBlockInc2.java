package incrementators;

public class SyncBlockInc2 implements DefaultIncrementation {

    private Integer i = 0;
    private  final  Object lock=new Object();

    @Override
    public void incrementation(int count) {
        synchronized (lock) {
            i += count;
        }
    }

    @Override
    public int getData() {
        return i;
    }

}
