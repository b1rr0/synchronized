package incrementators;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicInc implements DefaultIncrementation {

    private static AtomicInteger i = new AtomicInteger(0);

    @Override
    public void incrementation(int count) {
        i.addAndGet(count);
    }

    @Override
    public int getData() {
        return i.get();
    }

}
