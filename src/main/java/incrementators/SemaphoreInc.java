package incrementators;

import java.util.concurrent.Semaphore;

public class SemaphoreInc implements DefaultIncrementation {

    private int i = 0;
    private Semaphore semaphore;

    public SemaphoreInc(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void incrementation(int count) {
        try {
            semaphore.acquire();
            i += count;
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public int getData() {
        return i;
    }

}
