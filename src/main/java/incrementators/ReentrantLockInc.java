package incrementators;

import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockInc
        implements DefaultIncrementation {

    private int i = 0;
    private final ReentrantLock locker = new ReentrantLock();

    @Override
    public  synchronized void incrementation(int count) {
        locker.lock();
        i += count;
        locker.unlock();
    }

    @Override
    public int getData() {
        return i;
    }

}
