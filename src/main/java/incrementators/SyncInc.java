package incrementators;

public class SyncInc implements DefaultIncrementation {

    private int i = 0;

    @Override
    public synchronized void incrementation(int count) {
        i += count;
    }

    @Override
    public int getData() {
        return i;
    }

}
