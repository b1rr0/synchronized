package incrementators;

public interface DefaultIncrementation {

    void incrementation(int count);

    int getData();
}
