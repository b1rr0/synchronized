import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import runners.PhaserThreadRunner;


public class PhaserTest {


    private static final PhaserThreadRunner runner = new PhaserThreadRunner();
    @Test
    public void PhaserBlock() {
        // (1+2+3+4+5)*1*2*3*4*5=1800
        long res = runner.getAns();
        Assertions.assertEquals(1800, res);
    }


}
