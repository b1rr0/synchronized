import incrementators.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import runners.IncrementThreadRunner;

import java.util.concurrent.Semaphore;


class DataIncrementTest {

    private static final int RESULT = 50_000;
    private static final IncrementThreadRunner runner = new IncrementThreadRunner();

    @Test
    void DefaultIncrement() {
        int res = runner.getIncrementValueFromImplementation(new DefaultIntInc());
        Assertions.assertNotEquals(RESULT, res);
    }

    @Test
    void AtomicIntegerIncrement() {
        int res = runner.getIncrementValueFromImplementation(new AtomicInc());
        Assertions.assertEquals(RESULT, res);
    }

    @Test
    void SyncIncrement() {
        int res = runner.getIncrementValueFromImplementation(new SyncInc());
        Assertions.assertEquals(RESULT, res);
    }

    @Test
    void SyncBlockIncrement() {
        int res = runner.getIncrementValueFromImplementation(new SyncBlockInc());
        Assertions.assertEquals(RESULT, res);
    }

    @Test
    void ReentrantLockIncrement() {
        int res = runner.getIncrementValueFromImplementation(new ReentrantLockInc());
        Assertions.assertEquals(RESULT, res);
    }

    @Test
    void SyncBlockIncrement2() {
        int res = runner.getIncrementValueFromImplementation(new SyncBlockInc2());
        Assertions.assertEquals(RESULT, res);
    }

    @Test
    void SemaphoreIncrement2() {
        int res = runner.getIncrementValueFromImplementation(new SemaphoreInc(new Semaphore(1)));
        Assertions.assertEquals(RESULT, res);
    }
}
